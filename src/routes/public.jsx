import React, { Component } from "react";
import { Route, Routes } from "react-router-dom";
import Product from "../pages/products";
import Home from "../pages/home";
import Dashboard from "../pages/dashboard";

class Public extends Component {
  render() {
    return (
      <Routes>
        <Route path="/" exact element={<Home />} />
        <Route path="/dashboard" element={<Dashboard />} />
        <Route path="/products" element={<Product />} />
      </Routes>
    );
  }
}
export default Public;
