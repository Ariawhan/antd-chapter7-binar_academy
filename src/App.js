import Navbar from "./components/navbar";
import React from "react";
import "./App.css";
import Public from "./routes/public";
import { BrowserRouter } from "react-router-dom";

function App() {
  const webName = "Ariawan Web";
  return (
    <>
      <BrowserRouter>
        <div className="App">
          <main className="container">
            <Navbar webName={webName}></Navbar>
            <Public />
          </main>
        </div>
      </BrowserRouter>
    </>
  );
}

export default App;
