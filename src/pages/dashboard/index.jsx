import React, { Component } from "react";
import { Table } from "antd";
import dataDummy from "../../data/product.json";
import Btn from "../../components/btn";

class Dashboard extends Component {
  handelDalete = (cardsId) => {
    const data = [];
    for (let i = 0; i < this.state.cardsData.length; i++) {
      if (this.state.cardsData[i].id !== cardsId) {
        data.push(this.state.cardsData[i]);
      }
    }
    this.setState({ cardsData: data });
    console.log(data);
  };

  state = {
    tableData: dataDummy,
    columns: [
      {
        title: "Id",
        width: 100,
        dataIndex: "id",
        key: "id",
        fixed: "left",
      },
      {
        title: "Title",
        width: 100,
        dataIndex: "title",
        key: "title",
        fixed: "left",
      },
      {
        title: "Description",
        width: 100,
        dataIndex: "description",
        key: "description",
        fixed: "left",
      },
      {
        title: "Price",
        width: 100,
        dataIndex: "price",
        key: "price",
        fixed: "left",
      },
      {
        title: "Stock",
        width: 100,
        dataIndex: "stock",
        key: "stock",
        fixed: "left",
      },
      {
        title: "Action",
        key: "id",
        fixed: "right",
        // dataIndex: "id",
        width: 100,
        render: () => (
          <div className="row">
            <Btn
              type="Click"
              btn={() => this.state.onDelete(this.state.id)}
              btnName={"delete"}
              style="btn btn-danger"
              // id={this.props.dataSource.id}
              //Saya butuh id di sini :(
            />
            <Btn
              type="PopUp"
              btnName="Images"
              style="btn btn-primary"
              // id={this.key}
            />
          </div>
        ),
      },
    ],
  };
  render() {
    return (
      <div className="container">
        <div className="mt-3">
          <Table
            dataSource={this.state.tableData}
            columns={this.state.columns}
            pagination={{ pageSize: 5 }}
            scroll={{ y: 240 }}
          />
        </div>
      </div>
    );
  }
}

export default Dashboard;
