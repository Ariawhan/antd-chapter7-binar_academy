import React, { Component } from "react";
import { Button } from "antd";
import { Link } from "react-router-dom";

class Home extends Component {
  render() {
    return (
      <div className="row text-center mt-5">
        <div className="col">
          <Link to="/products">
            <Button type="primary" shape="round" size="large">
              Products
            </Button>
          </Link>
        </div>
        <div className="col">
          <Link to="/dashboard">
            <Button type="dashed" shape="round" size="large">
              Dashboard
            </Button>
          </Link>
        </div>
      </div>
    );
  }
}

export default Home;
